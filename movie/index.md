---
layout: list
title: 다양한 영화 정보
slug: movie
permalink: /movie
description: >
  다양한 종류의 영화 정보들을 공유합니다.
tags:
  - movie
  - 영화
  - 영화 소개
pagination:
  enabled: true
  category: movie
---
