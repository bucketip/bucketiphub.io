var copyToClipboard = function (url) {
  var input = document.getElementById('copy-target-input');
  input.value = url;
  input.hidden = false;
  input.select();
  document.execCommand('copy');
  input.hidden = true;
  var toast = document.querySelector('.toast-alert');
  if (toast) {
    toast.classList.remove('active');
    void toast.offsetWidth;
    toast.classList.add('active');
  }
};

(function () {
  var qObject = {};
  var queries = window.location.search.substr(1).split('&');
  for (i in queries) {
    q = queries[i].split('=');
    qObject[decodeURIComponent(q[0])] = decodeURIComponent(q[1]);
  }
  if (qObject.share && qObject.share === 'kakao-talk') {
    loadKakaoSDK(false);
  }
  if (qObject.share && qObject.share === 'kakao-story') {
    loadKakaoSDK(true);
  }
  if (qObject.share && qObject.share === 'clipboard') {
    copyToClipboard(siteUrl + '/' + kakaoPath);
  }
  removeQueryString();
})();