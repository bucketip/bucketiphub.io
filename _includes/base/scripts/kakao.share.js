var isLoadedKakaoSDK = false;
var isLoadingKakaoSDK = false;

var loadKakaoSDK = function (isStory) {
  var script = document.createElement('script');
    script.setAttribute('src', '/assets/js/kakao.min.js');
    script.setAttribute('type', 'text/javascript');
    script.addEventListener('load', function(e) {
      Kakao.init('8243fa8a2a781c5cf303a343dc208bc8');
      isLoadedKakaoSDK = true;
      isLoadingKakaoSDK = false;
      const api = isStory ? shareKakaoStory : shareKakaoTalk;
      api();
    });
    isLoadingKakaoSDK = true;
    document.head.appendChild(script);
};

var shareKakaoStory = function () {
  if (!isLoadedKakaoSDK && !isLoadingKakaoSDK) {
    loadKakaoSDK(true);
  }

  if (isLoadedKakaoSDK) {
    Kakao.Story.share({
      url: siteUrl + '/' + kakaoPath,
      text: kakaoTitle + '\n\n' + kakaoDescription
    });
  }
}

var shareKakaoTalk = function () {
  if (!isLoadedKakaoSDK && !isLoadingKakaoSDK) {
    loadKakaoSDK(false);
  }
  if (isLoadedKakaoSDK) {
    Kakao.Link.sendCustom({
      templateId: 39838,
      templateArgs: {
        thumbnail: kakaoThumbnail,
        title: kakaoTitle,
        description: kakaoDescription,
        path: kakaoPath
      }
    });
  }
};
