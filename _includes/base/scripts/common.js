var switchExpand = function (node, className) {
  if (node && node.classList.contains(className)) {
    node.classList.remove(className)
  } else {
    node.classList.add(className)
  }
}
