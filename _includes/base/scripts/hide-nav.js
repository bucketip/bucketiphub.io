var prevScrollpos = window.pageYOffset;

window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
  var targets = [
    {
    target: document.querySelector(".navbar"),
    direction: 'top',
    size: -60
    }
  ];
  targets.forEach(function (t) {
    var show = currentScrollPos < 30 || prevScrollpos > currentScrollPos
    setHideStyle(t.target, t.direction, show ? 0 : t.size)
  });
  prevScrollpos = currentScrollPos;
}

var setHideStyle = function (target, direction, size) {
  if (target) {
    target.style[direction] = size + 'px';
  }
}