document.querySelector('.share-toggle').addEventListener('click', function () {
  switchExpand(document.querySelector('.share'), 'active');
});

var removeQueryString = function () {
  var newURL = location.href.split("?")[0];
  window.history.replaceState(null, null, newURL);
}