---
layout: main
title: Home
slug: home
permalink: /
description: >
  영화, 유용한 사이트, 게임 그리고 각 주제 별 랭킹을 다루는 블로그입니다.
pagination:
  enabled: true
---
